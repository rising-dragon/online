function testJson() {
    var sent = $("#boxText").val();
    $.ajax({
        url: "/dynamic",
        type: "POST",
        dataType: "text",
        data: {sent:sent},
        success: function (result) {
            if (""==sent) {
                alert("请输入内容！");
                $('#reBoxText').html("");
            } else {
                $('#reBoxText').html(result);
            }
        }
    });
}


/*清除结果*/
function clearResult() {
    $('#reBoxText').html("");
}

/*文本框显示行号*/
/*start*/
String.prototype.trim2 = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
function F(objid) {
    return document.getElementById(objid).value;
}
function G(objid) {
    return document.getElementById(objid);
}
var num="";
function keyUp() {
    var obj=G("boxText");
    var str=obj.value;
    str=str.replace(/\r/gi,"");
    str=str.split("\n");
    n=str.length;
    line(n);
}
function line(n) {
    var lineobj=G("li");
    for(var i=1;i<=n;i++){
        if(document.all){
            num+=i+"\r\n";
        }else{
            num+=i+"\n";
        }
    }
    lineobj.value=num;
    num="";
}
function autoScroll() {
    var nV = 0;
    if(!document.all){
        nV=G("boxText").scrollTop;
        G("li").scrollTop=nV;
        setTimeout("autoScroll()",20);
    }
}
if(!document.all){
    window.addEventListener("load",autoScroll,false);
}

function keyHandler(e) {
    var TABKEY = 9;
    str = "    ";
    if(event.keyCode == TABKEY) {
        insertText(e, str);
        if(event.preventDefault) {
            event.preventDefault();
        }
    }
}

function insertText(obj,str) {
    if(document.selection) {
        var sel = document.selection.createRange();
        sel.text = str;
    }else if(typeof obj.selectionStart === 'number' && typeof obj.selectionEnd === 'number') {
        var startPos = obj.selectionStart,
            endPos = obj.selectionEnd,
            cusorPos = startPos,
            tmpStr = obj.value;
        obj.value = tmpStr.substring(0,startPos) + str + tmpStr.substring(endPos, tmpStr.length);
        cusorPos += str.length;
        obj.selectionStart = obj.selectionEnd = cusorPos;
    } else {
      obj.value += str;
    }
}
