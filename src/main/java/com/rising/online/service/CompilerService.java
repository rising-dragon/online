package com.rising.online.service;


import com.rising.online.bean.CompilerBean;

/**
 * @Description   : 编译service
 * @Auther        : rising
 * @CreateDate    : 2019-09-16 23:13
 */
public interface CompilerService {

    /**
     * 编译方法
     * @param compilerBean
     * @return
     */
    public boolean compile(CompilerBean compilerBean);

    /**
     * 运行主方法
     * @param compilerBean
     */
    public void runMain(CompilerBean compilerBean);
}
