package com.rising.online.bean;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import java.io.IOException;


/**
 * @Description   : java文件管理器
 * @Auther        : rising
 * @CreateDate    : 2019-09-16 23:13
 */
public class DynamicCompileJavaFileManager extends ForwardingJavaFileManager {

    private CompilerBean compilerBean;
    public DynamicCompileJavaFileManager(JavaFileManager fileManager, CompilerBean compilerBean) {
        super(fileManager);
        this.compilerBean = compilerBean;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
        DynamicCompileJavaFileObject javaFileObject = new DynamicCompileJavaFileObject(compilerBean, className, kind);
        return javaFileObject;
    }
}
