package com.rising.online.bean;

import javax.tools.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 编译bean实体
 */
public class CompilerBean {
    //类全名
    private String fullClassName;
    //源文件
    private String sourceCode;

    private Map<String, byte[]> classBytes = new HashMap<>();
    //获取java的编译器
    private JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    //存放编译过程中输出的信息
    private DiagnosticCollector<JavaFileObject> diagnosticCollector = new DiagnosticCollector<>();
    //存放控制台输出内容
    private String runResult;
    //编译耗时（单位ms）
    private long compilerTime;
    //运行耗时（单位ms）
    private long runTime;

    public CompilerBean() {
    }

    public CompilerBean(String fullClassName, String sourceCode, String runResult, long compilerTime, long runTime) {
        this.fullClassName = fullClassName;
        this.sourceCode = sourceCode;
        this.runResult = runResult;
        this.compilerTime = compilerTime;
        this.runTime = runTime;
    }

    public String getFullClassName() {
        return fullClassName;
    }

    public void setFullClassName(String fullClassName) {
        this.fullClassName = fullClassName;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public Map<String, byte[]> getClassBytes() {
        return classBytes;
    }

    public void setClassBytes(Map<String, byte[]> classBytes) {
        this.classBytes = classBytes;
    }

    public JavaCompiler getCompiler() {
        return compiler;
    }

    public void setCompiler(JavaCompiler compiler) {
        this.compiler = compiler;
    }

    public DiagnosticCollector<JavaFileObject> getDiagnosticCollector() {
        return diagnosticCollector;
    }

    public void setDiagnosticCollector(DiagnosticCollector<JavaFileObject> diagnosticCollector) {
        this.diagnosticCollector = diagnosticCollector;
    }

    public String getRunResult() {
        return runResult;
    }

    public void setRunResult(String runResult) {
        this.runResult = runResult;
    }

    public long getCompilerTime() {
        return compilerTime;
    }

    public void setCompilerTime(long compilerTime) {
        this.compilerTime = compilerTime;
    }

    public long getRunTime() {
        return runTime;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    /**
     * @return  编译信息（错误，警告）
     */
    public String getCompilerMessage() {
        StringBuilder sb = new StringBuilder();
        List<Diagnostic<? extends JavaFileObject>> diagnostics = diagnosticCollector.getDiagnostics();
        for (Diagnostic diagnostic : diagnostics) {
            sb.append(diagnostic.toString()).append("\r\n");
        }
        return sb.toString();
    }
}
