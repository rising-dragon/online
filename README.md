# online

#### 介绍
在线编译java代码，利用动态编译原理，实现在网页上写代码，后台编译并输出结果给用户

#### 软件架构
软件架构说明


#### 安装教程

1.  电脑需要安装maven、git等工具
2.  在idea或者eclipse中下载此项目，确保环境没有问题
3.  直接运行OnlineApplication，在浏览器中输入http://127.0.0.1:30100/index.html或者http://127.0.0.1:30100即可使用
4.  也可在application.properties里面自定义端口号

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
